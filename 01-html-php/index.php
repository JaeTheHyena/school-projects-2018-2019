<!DOCTYPE lang="en" html>
<html>

<!--
	No Separate CSS file because this exerice is ultra-easy.
 -->

<head>
	<title>PHP 01</title>
	<meta charset="utf-8">
</head>

<body>
	<?php
		print('This text is printed with PHP ! <br>');

		echo "Today is " . date("Y/m/d") . "<br>";

	?>

	<h1>We can include HTML in the PHP file !</h1>

	<style>
	body {
			text-align: center;
			color: white;
			background-color: grey;
		}
	</style>
</body>

</html>
